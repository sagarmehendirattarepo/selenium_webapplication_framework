package CircuitSearch;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import BaseTest.BaseTest;
import PageObjects.circuitSearchPage;
import PageObjects.ecmsCircuitGeneralPresentationPage;
import Utils.ExcelRead;
import Utils.PropertyManager;
import Utils.WebelementsCommands;
import Utils.WindowHelper;

public class CircuitSearch_with_CircuitStatus_OnCancellation_TestCase extends BaseTest{
	
	PropertyManager PropertyManager=new PropertyManager();
	WebelementsCommands bCommands=new WebelementsCommands();
	ExcelRead ExcelRead=new ExcelRead();
	WindowHelper WindowHelper=new WindowHelper();
	BaseTest baseTest=new BaseTest();
	
	boolean cancellationstatus;
	String Cstatus;
	
	@Test(priority=1,groups= {"sanity","Regression"})
	public void Circuits_CircuitSearch_with_CircuitStatus_OnCancellation() throws IOException, InterruptedException {
		
		baseTest.LoginCCR(driver);
		Cstatus="On Cancellation";
		
		circuitSearchPage circuitSearchPageObj = PageFactory.initElements(driver, circuitSearchPage.class);
		bCommands.WaitForElementToVissible(driver,circuitSearchPageObj.CircuitSearchBtn, 120);
		
		bCommands.dropdownbyvissibletext(driver, circuitSearchPageObj.verify_DDWebelement(driver, "stateIdA"), Cstatus);
		bCommands.waitForSec();
		
		bCommands.WebelementClickOperation(driver, circuitSearchPageObj.CircuitSearchBtn);
		bCommands.WaitForElementToVissible(driver,circuitSearchPageObj.circuitIdHyperlink, 180);
		
		cancellationstatus=circuitSearchPageObj.SearchResulttable_data(driver, Cstatus);
		
		if(cancellationstatus) {
			assert(true);
		}else {
			assert(false);
		}		
		
	}
	
	@Test(priority=2,groups= {"sanity","Regression"},dependsOnMethods= {"Circuits_CircuitSearch_with_CircuitStatus_OnCancellation"})
	public void CircuitSearch__Open_and_verify_Searched_circuit_for_status_Oncancellation() throws IOException, InterruptedException {
		
		String statusText;
		circuitSearchPage circuitSearchPageObj = PageFactory.initElements(driver, circuitSearchPage.class);
		ecmsCircuitGeneralPresentationPage ecmsCircuitGeneralPresentationPageObj=PageFactory.initElements(driver, ecmsCircuitGeneralPresentationPage.class);
		
		bCommands.WebelementClickOperation(driver, circuitSearchPageObj.circuitIdHyperlink);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		bCommands.WaitForElementToVissible(driver,ecmsCircuitGeneralPresentationPageObj.CircuitReference, 120);
		
		statusText=ecmsCircuitGeneralPresentationPageObj.StatusSubStatusText.getAttribute("value");
		System.out.println("statusText "+statusText);
		if(statusText.contains(Cstatus)) {
			assert(true);
		}else {
			assert(false);
		}
		
		bCommands.waitForSec();
		//driver.close();		
	}
	
	@AfterTest
	public void closeBroser() {
		driver.close();	
		
	}
	
}
