package BaseTest;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import PageObjects.ccrLoginPage;
import PageObjects.circuitSearchPage;
import Utils.Browser;
import Utils.ExtentManager;
import Utils.PropertyManager;
import Utils.Sendemailnotification;
import Utils.WebelementsCommands;
import Utils.Automail;



public class BaseTest extends ExtentManager  {
	
	protected  WebDriver driver;
	public  WebDriverWait wait;	
	String UserName;
	String Password;
	String URL;
	String WebBrowser;
	String Environment;
	public static File reportDirectery;
	protected  String pbrowser;
	
	public BaseTest() {	
		
		String log4jConfPath ="log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);	
		
	}	
	
	PropertyManager PropertyManager=new PropertyManager();
	Browser Browser=new Browser();
	Automail Automail=new Automail();

	
	//@Parameters({"webbrowser"})
	@BeforeTest()
	public  void BrowserInitialization() throws InterruptedException, MalformedURLException {	
		
		WebBrowser=PropertyManager.getInstance().getBrowser();
		//URL=PropertyManager.getInstance().getURL();	
		//UserName=PropertyManager.getInstance().getUsername();	
		//Password=PropertyManager.getInstance().getPassword();
		
		//pbrowser=webbrowser;
		
		driver=Browser.CallingWebBrowserDriver(WebBrowser);		
		wait=new WebDriverWait(driver, 60);
		driver.manage().window().maximize();			
		//driver.get(URL);			
		//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		//Thread.sleep(5000);
		//driver.navigate().refresh();
		/*
		 * ccrLoginPage ccrLoginPageObj = PageFactory.initElements(driver,
		 * ccrLoginPage.class); ccrLoginPageObj.UserName.sendKeys(UserName);
		 * ccrLoginPageObj.Password.sendKeys(Password);
		 * ccrLoginPageObj.LoginButton.click();
		 * System.out.println("Clicked on login button");
		 * driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		 * driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		 * System.out.println("Page is loaded succeffully");
		 */
		//driver.manage().window().maximize();
		
		}
	
	public void LoginCCR(WebDriver driver) throws InterruptedException {
		
		URL=PropertyManager.getInstance().getURL();	
		UserName=PropertyManager.getInstance().getUsername();	
		Password=PropertyManager.getInstance().getPassword();
		
		ccrLoginPage ccrLoginPageObj = PageFactory.initElements(driver, ccrLoginPage.class);
		circuitSearchPage ccrSearchPageObj = PageFactory.initElements(driver, circuitSearchPage.class);
		
		driver.get(URL);	
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		Thread.sleep(10000);
		ccrLoginPageObj.UserName.sendKeys(UserName);		
		ccrLoginPageObj.Password.sendKeys(Password);		
		ccrLoginPageObj.LoginButton.click();
		System.out.println("Clicked on login button");
		//driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
		Thread.sleep(5000);
		Thread.sleep(10000);
		//System.out.println("Page is loaded succeffully");
		//ccrSearchPageObj.loadingWait(driver);
		//ccrSearchPageObj.waitForLoaderTobeInvisible();
		//WebelementsCommands bCommands = new WebelementsCommands();
		//bCommands.waitForElementNotPresent(driver, ccrLoginPageObj.LoginButton, 60);
		
	}
	//div[@class='lds-span']
	
	//*[local-name()='svg' and @data-icon='home']/*[local-name()='path']
	
		/*
		 * @AfterTest(groups= {"smoke","sanity","Regression"}) public void teardown() {
		 * 
		 * driver.quit(); }
		 */
	
	/*
	 * @AfterSuite(groups= {"smoke","sanity","Regression"}) public void
	 * teardownallBrowser() throws AddressException, InvalidFormatException,
	 * InterruptedException, IOException, MessagingException { Sendemailnotification
	 * Automail=new Sendemailnotification(); driver.quit();
	 * Automail.Emailed_Reports();
	 * 
	 * }
	 */
	
	
	public WebDriver getDriver() {
        return driver;
    }
	
	public String getbrowser() {
        return pbrowser;
    }
	
	
	
	

}
